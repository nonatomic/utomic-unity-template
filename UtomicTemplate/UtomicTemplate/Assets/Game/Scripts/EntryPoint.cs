﻿using UnityEngine;
using Nonatomic.Utomic;
using System.Collections;

namespace Nonatomic.Game {

	public class EntryPoint : MonoBehaviour {

		private void Awake() {

			this.AddUtomic();

			/**
			* Features
			*
			* Any game feature that requires wiring should be wrapped
			* as a feature and added here.
			*
			* Features are responsible for wiring themselves into Utomic
			*/
			this.AddFeature<PreloaderFeature>();
			this.AddFeature<Setup2DFeature>();
			this.AddFeature<StartFeature>();
			this.AddFeature<GameFeature>();
            this.AddFeature<ScoreFeature>();
		}
	}
}
