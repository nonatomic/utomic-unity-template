//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;

namespace Nonatomic.Game {

	public class ScoreMenu : IMenu {

		private GameObject container;
		private IPrefabFactory prefabFactory;
		private IUIFactory uiFactory;
		private GameObject panel;

		public ScoreMenu(){

			prefabFactory = this.Inject<IPrefabFactory>();
			uiFactory = this.Inject<IUIFactory>();

			var canvas = this.EntityList().GetEntity<UICanvasComponent>();
			container = uiFactory.MakeContainer("ScoreMenu", canvas);
            
            var scoreLabel = prefabFactory.Make("ScoreLabel", container);

			container.SetActive(false);

		}
        
		public void Show(MenuState state) {

			container.SetActive(true);
		}

		public void Hide(MenuState state) {
			container.SetActive(false);
		}
	}
}
