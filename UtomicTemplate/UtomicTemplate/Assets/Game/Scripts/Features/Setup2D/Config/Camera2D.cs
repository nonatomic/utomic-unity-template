//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Game {

	public class Camera2D : CameraConfig{

		public Camera2D() {
			name = "Camera2D";
			orthographic = true;
			color = new Color32(30, 30, 30, 255);
			clearFlags = CameraClearFlags.Skybox;
			nearClipPlane = -1f;
			farClipPlane = 100f;
			orthographicSize = 5f;
			cullingMask = 1 << LayerMask.NameToLayer("Default");
		}
	}
}
