//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;

namespace Nonatomic.Game {

	public class StartMenu : IMenu {

		private GameObject container;
		private IPrefabFactory prefabFactory;
		private IUIFactory uiFactory;
		private GameObject panel;

		public StartMenu(){

			prefabFactory = this.Inject<IPrefabFactory>();
			uiFactory = this.Inject<IUIFactory>();

			var canvas = this.EntityList().GetEntity<UICanvasComponent>();
			container = uiFactory.MakeContainer("StartMenu", canvas);

			panel = prefabFactory.Make("StartPanel", container);
			var startButton = prefabFactory.MakeButton("StartButton", panel, this.Inject<StartButtonSignal>());

			container.SetActive(false);

		}

		public void Show(MenuState state) {

			container.SetActive(true);

			var t = panel.gameObject.RectTransform();
			t.SetPosition(0,-1000);
			LeanTween.moveY(t, 0, 0.5f).setEase(LeanTweenType.easeInBack);

		}

		public void Hide(MenuState state) {
			container.SetActive(false);

			LeanTween.moveY(panel.gameObject.RectTransform(), -1000, 0.5f).setEase(LeanTweenType.easeOutBack);
		}
	}
}
