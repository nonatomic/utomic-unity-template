//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Match3 {

	public class PayloadClickCommand : ICommand {

		public void Execute(params object[] parameters){

			var source = (GameObject)parameters[0];
			var grid = this.Inject<IMatch3Grid>();
			var element = grid.Find(x => x.Payload.Sprite == source);
			this.Log(element.x + "," + element.y + "," + element.Payload.Sprite.transform.position.y);

			var model = this.Inject<IMatch3Model>();

			this.Inject<HighlightPayloadSignal>().Dispatch(element);

			if(model.Selected.Count < 2){
				model.Selected.Add(element);
			}

			if(model.Selected.Count == 2){
				this.Inject<SwitchPayloadsSignal>().Dispatch(model.Selected[0], model.Selected[1]);
			}
		}
	}
}
