//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Match3 {

	public class HighlightPayloadCommand : ICommand {

		private IMatch3Factory factory;

		public void Execute(params object[] parameters){

			var element = (Match3Element) parameters[0];
			element.selected = true;
		
			factory = factory ?? this.Inject<IMatch3Factory>();
			factory.MakePayloadHighlight(element.Payload);
		}
	}
}
