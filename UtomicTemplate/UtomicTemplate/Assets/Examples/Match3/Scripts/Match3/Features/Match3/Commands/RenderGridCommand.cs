//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Match3 {

	public class RenderGridCommand : ICommand {

		private IMatch3Factory factory;
		private GameObject canvas;
		private GameObject container;

		public void Execute(params object[] parameters) {

			canvas = canvas ?? this.EntityList().GetEntity<SpriteCanvasComponent>();
			factory = factory ?? this.Inject<IMatch3Factory>();
			container = container ?? new GameObject("Match3 Container");
			container.transform.SetParent(canvas.transform);

			var grid = this.Inject<IMatch3Grid>();
			var elements = grid.All();
			var size = 0.56f;
			var gap = 0.01f;
			var sizeGap = size + gap;
			var startX = (sizeGap * (grid.Width - 1)) * 0.5f;
			var startY = (sizeGap * (grid.Height - 1)) * 0.5;

			foreach(var element in elements){

				//if payloads sprite is empty lets make one
				if(element.Payload != null && element.Payload.Sprite == null){
					factory.MakePayloadSprite(container, element.Payload);
				}

				//position payload
				var x = -startX + (element.gridPos.x * sizeGap);
				var y = startY - (element.gridPos.y * sizeGap);
				var pos = new Vector2((float)x, (float)y);
				element.Payload.Sprite.transform.localPosition = pos;

			}
		}
	}
}
