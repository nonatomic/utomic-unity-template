//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using Nonatomic.Utomic;

namespace Nonatomic.Match3 {

	public class BuildGameFSMCommand : ICommand {

		public void Execute(params object[] parameters) {

			//state machine based match3
			var fsm = new FSM();
			fsm.AddTransition<PauseState, PauseTrigger>(); //global transition

			fsm.AddState<BuildGridState>(true)
			.AddTransition<NoneSelectedState, CompleteTrigger>();

			fsm.AddState<NoneSelectedState>()
			.AddTransition<SelectedState, SelectTileTrigger>();

			fsm.AddState<SelectedState>()
			.AddTransition<SelectedState, SelectNoneAdjacentTileTrigger>() //re-enter the state with the new tile
			.AddTransition<SwapAnimationState, SelectAdjacentTileTrigger>();

			fsm.AddState<SwapAnimationState>()
			.AddTransition<CheckLegalSwapState, CompleteTrigger>();

			fsm.AddState<CheckLegalSwapState>()
			.AddTransition<CheckForMatchesState, LegalMoveTrigger>()
			.AddTransition<SwapBackAnimationState, ILLegalMoveTrigger>();

			fsm.AddState<SwapBackAnimationState>()
			.AddTransition<NoneSelectedState, CompleteTrigger>();

			fsm.AddState<CheckForMatchesState>()
			.AddTransition<RemoveMatchesState, MatchesFoundTrigger>()
			.AddTransition<NoneSelectedState, NoMatchesFoundTrigger>();

			fsm.AddState<RemoveMatchesState>()
			.AddTransition<CheckForMatchesState, CompleteTrigger>();

			fsm.AddState<PauseState>()
			.AddBackTrigger<ResumeTrigger>();

		}
	}
}
