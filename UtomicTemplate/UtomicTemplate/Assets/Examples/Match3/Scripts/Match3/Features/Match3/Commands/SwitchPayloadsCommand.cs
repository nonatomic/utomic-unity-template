//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using Nonatomic.Utomic;
using UnityEngine;

namespace Nonatomic.Match3 {

	public class SwitchPayloadsCommand : ICommand {

		public void Execute(params object[] parameters) {

			var model = this.Inject<IMatch3Model>();
			var element1 = (Match3Element) parameters[0];
			var element2 = (Match3Element) parameters[1];

			//1.0 so we need to check if the two elements are neighbours
				//1.1 elements are neighbours - switch places if matches can be made
					//1.1.1 if matches can be made switch
					//1.1.2 if no matches can be made perform simulated switch (aka do nothing)
				//1.2 elements aren't neighbours - reset Match3Model Selected list

			var isNeighbour = element1.IsNeighbour(element2);

			if(isNeighbour){
				//switch places - if matches can be made

				//count matches
				//switch places
				//count matches
				//if diff > 1 switch places
				var grid = this.Inject<IMatch3Grid>();
				var matchesBefore = grid.MatchCount();

				//switch pos
				grid.SwitchPayloads(element1, element2);
				var rowMatchQuery = new RowMatchQuery();
				var colMatchQuery = new ColumnMatchQuery();
				rowMatchQuery.Run(grid);
				colMatchQuery.Run(grid);
				var matchesAfter = grid.MatchCount();

				//swapped permanently
				if(matchesAfter > matchesBefore){

					this.Inject<RenderGridSignal>().Dispatch();

					//reset selection count
					this.Inject<IMatch3Model>().Selected.Clear();
				}

				//reverse swap
				else{
					//reverse swap
					grid.SwitchPayloads(element1, element2);

					//remove highlights
					this.Inject<RemoveAllPayloadHighlightsSignal>().Dispatch();

					//reset selection count
					this.Inject<IMatch3Model>().Selected.Clear();

					this.Log("SWAP BACK", Color.red);
				}
			}
			else{

				//move 2nd Selected object to first
				var e2 = model.Selected[1];
				model.Selected.Clear();
				model.Selected.Add(e2);
				this.Log("RESET SELECTION:" + model.Selected.Count);

			}

		}
	}
}
