//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Match3 {

	public class Match3Feature : IFeature{

		public void Wire() {

			this.CommandMap().Bind<BuildGameFSMSignal, BuildGameFSMCommand>();

			this.CommandMap().Bind<BuildGridSignal, BuildGridCommand>();
			this.CommandMap().Bind<RenderGridSignal, RenderGridCommand>();
			this.CommandMap().Bind<PayloadClickSignal, PayloadClickCommand>();
			this.CommandMap().Bind<SwitchPayloadsSignal, SwitchPayloadsCommand>();
			this.CommandMap().Bind<HighlightPayloadSignal, HighlightPayloadCommand>();
			this.CommandMap().Bind<RemoveAllPayloadHighlightsSignal, RemoveAllPayloadHighlightsCommand>();

			this.Bind<IMatch3Factory, Match3Factory>();
			this.Bind<IMatch3Grid, Match3Grid>();
			this.Bind<IMatch3Model, Match3Model>();
		}

		public void Structure(){
			//do nothing
		}

		public void Setup(){
			this.Inject<IMenuManager>().AddMenu(new Match3Menu());
		}

		public void Run(){
			this.Inject<BuildGameFSMSignal>().Dispatch();
			// this.Inject<BuildGridSignal>().Dispatch();
			// this.Inject<SetupGameStateMachineSignal>().Dispatch();
		}
	}
}
