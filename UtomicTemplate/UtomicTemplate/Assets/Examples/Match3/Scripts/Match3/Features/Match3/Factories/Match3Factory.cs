//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Match3 {

	public class Match3Factory : IMatch3Factory {

		private ISpriteFactory spriteFactory;

		public Match3Factory() {

			spriteFactory = this.Inject<ISpriteFactory>();
		}

		public GameObject MakePayloadSprite(GameObject parent, IMatch3Payload payload) {

			payload.Sprite = spriteFactory.MakeSprite(payload.ImagePath, parent).SetPositionZ(1);
			payload.Sprite.AddEntityComponent<BoxCollider2D>();

			var monitor = payload.Sprite.AddEntityComponent<PayloadClickComponent>();

			return payload.Sprite;
		}

		public GameObject MakePayloadHighlight(IMatch3Payload payload) {
			
			return spriteFactory.MakeSpriteFromPool<PayloadHighlightComponent>("Selected", payload.Sprite).gameObject;
		}
	}
}
