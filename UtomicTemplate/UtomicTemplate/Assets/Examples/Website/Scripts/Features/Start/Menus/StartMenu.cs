//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;

namespace Nonatomic.Website {

	public class StartMenu : IMenu {

		private GameObject container;
		private IPrefabFactory prefabFactory;
		private IUIFactory uiFactory;
		private GameObject panel;

		public StartMenu(){

			prefabFactory = this.Inject<IPrefabFactory>();
			uiFactory = this.Inject<IUIFactory>();

			var canvas = this.EntityList().GetEntity<UICanvasComponent>();
			container = uiFactory.MakeContainer("StartMenu", canvas);

			/**
			* Example of building menus via either prefab or code
			* Code option is more flexible, but by prefab is quicker
			* for rapid prototyping.
			*
			* When building prefabs I recommend only using builtin Unity scripts
			* on the prefab.
			*/
			// MakeMenuUsingPrefabs();
			MakeMenuUsingCode();

			container.SetActive(false);

		}

		private void MakeMenuUsingPrefabs(){

			panel = prefabFactory.Make("StartPanel", container);
			var startButton = prefabFactory.MakeButton("StartButton", panel, this.Inject<StartButtonSignal>());
		}

		private void MakeMenuUsingCode(){
			MakePanel();
			MakeStartButton();
		}

		private void MakePanel(){

			var panelImg = uiFactory.MakeImage("Square", container);
			panelImg.gameObject.RectTransform().SetAnchorCenter().SetSize(608, 380);
			panelImg.color = new Color32(255, 255, 255, 170);
			panelImg.preserveAspect = false;
			panelImg.name = "Panel";

			panel = panelImg.gameObject;
		}

		private void MakeStartButton(){

			var buttonStyle = new TextStyle(){text = "Start", bestFit = true, color = new Color32(255,255,255,255)};
			var buttonTuple = uiFactory.MakeTextButton("Square", panel, this.Inject<StartButtonSignal>(), buttonStyle);
			var buttonImage = buttonTuple.Item1.GetComponent<Image>();
			buttonImage.preserveAspect = false;
			buttonImage.color = new Color32(255,0,0,255);
			buttonTuple.Item1.RectTransform().SetAnchorCenter().SetPosition(0,-114).SetSize(310, 84);
		}

		public void Show(MenuState state) {

			container.SetActive(true);

			var t = panel.gameObject.RectTransform();
			t.SetPosition(0,-1000);
			LeanTween.moveY(t, 0, 0.5f).setEase(LeanTweenType.easeInBack);

		}

		public void Hide(MenuState state) {
			container.SetActive(false);

			LeanTween.moveY(panel.gameObject.RectTransform(), -1000, 0.5f).setEase(LeanTweenType.easeOutBack);
		}
	}
}
