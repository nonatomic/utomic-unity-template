﻿using UnityEngine;
using System.Collections;

public class Bob : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Tween();
	}

	void Tween() {
		LeanTween.moveY(gameObject, -0.5f, 8f).setEase(LeanTweenType.easeOutElastic).setOnComplete(()=>{
			LeanTween.moveY(gameObject, -1.4f, 8f).setEase(LeanTweenType.easeOutElastic).setOnComplete(()=>{
				Tween();
			});
		});
	}
}
