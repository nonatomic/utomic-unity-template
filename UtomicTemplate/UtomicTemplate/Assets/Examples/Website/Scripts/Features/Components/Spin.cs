using UnityEngine;
using System.Collections;
using Nonatomic.Utomic;

public class Spin : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Rotate();
	}

	private void Rotate(){

		LeanTween.rotate(gameObject,new Vector3(0,-180,0), 0.2f).setOnComplete(()=>{
			LeanTween.rotate(gameObject,new Vector3(0,360,0), 0.2f).setOnComplete(()=>{
					this.Log("ROTATE COMPELTE");
					gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
				Rotate();
				});
			}).setDelay(8);
	}

}
