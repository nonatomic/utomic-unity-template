//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Website {

	public class Setup2DFeature : IFeature{

		public void Wire() {

		}

		public void Structure(){
			//To gain access to the root GameObject fetch the UtomicCore Entity
			var parent = this.EntityList().GetEntity<UtomicCore>();

			//The 2D Camera will render both GUI and Sprites
			this.Inject<ICameraFactory>().Make<Camera2DComponent>(new Camera2D(), parent);

			//The UI Canvas is a container for all UI
			this.Inject<ICanvasFactory>().Make<UICanvasComponent>(new UICanvas(), parent);

			//The Sprite Canvas is a container for all Sprites
			// var spriteCanvas = new GameObject("SpriteCanvas");
			// spriteCanvas.transform.SetParent(parent.transform, false);
			// spriteCanvas.AddEntityComponent<SpriteCanvasComponent>();
			// this.EntityList().AddEntity(spriteCanvas);
		}

		public void Setup(){


		}

		public void Run(){

		}
	}
}
