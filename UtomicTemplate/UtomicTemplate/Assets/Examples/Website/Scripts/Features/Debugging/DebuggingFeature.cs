//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Website {

	public class DebuggingFeature : IFeature{

		public void Wire() {

			//Factory
			this.Bind<IDebugFactory, DebugFactory>();
		}

		public void Structure(){
			//nothing
		}

		public void Setup(){
			//do nothing
		}

		public void Run(){

			var debugFactory = this.Inject<IDebugFactory>();
			debugFactory.MakeCanvas();
			debugFactory.MakeFPS();
			debugFactory.MakeEntityListCounter();
			debugFactory.MakeMemoryDisplay();
		}
	}
}
