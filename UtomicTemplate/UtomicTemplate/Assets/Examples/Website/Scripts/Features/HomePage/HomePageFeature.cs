//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Website {

	public class HomePageFeature : IFeature {

		public void Wire() {

		}

		public void Structure(){

		}

		public void Setup() {

		}

		public void Run() {

			var prefabFactory = this.Inject<IPrefabFactory>();
			var parent3d = this.EntityList().GetEntity<EntryPoint>();
			var parent2d = this.EntityList().GetEntity<UICanvasComponent>();
			prefabFactory.Make("Website", parent3d);
			prefabFactory.Make("Footer", parent2d);
		}
	}
}
