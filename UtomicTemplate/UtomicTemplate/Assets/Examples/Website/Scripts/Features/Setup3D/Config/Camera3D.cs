//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Website {

	public class Camera3D : CameraConfig{

		public Camera3D() {
			name = "Camera3D";
			orthographic = false;
			color = new Color32(202, 21, 150, 255);
			clearFlags = CameraClearFlags.Skybox;
			nearClipPlane = 0.3f;
			farClipPlane = 1000f;
			// orthographicSize = 5f;
			cullingMask = 1 << LayerMask.NameToLayer("Default");
			position = new Vector3(-0.027f,2.11f,-10f);
		}
	}
}
