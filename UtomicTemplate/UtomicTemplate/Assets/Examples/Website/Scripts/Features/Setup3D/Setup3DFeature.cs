//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Website {

	public class Setup3DFeature : IFeature{

		public void Wire() {

		}

		public void Structure(){
			//To gain access to the root GameObject fetch the UtomicCore Entity
			var parent = this.EntityList().GetEntity<UtomicCore>();
			var camera = this.Inject<ICameraFactory>().Make<Camera3DComponent>(new Camera3D(), parent);
		}

		public void Setup(){


		}

		public void Run(){

		}
	}
}
