//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Website {

	public class PreloaderFeature : IFeature {

		public void Wire() {

			//wire into the core preloader
			this.CommandMap().Bind<UtomicFeatureCompleteSignal, PreloadFeatureCompleteCommand>();
			this.CommandMap().Bind<UtomicLoadingCompleteSignal, PreloadAssetsCompleteCommand>();
			this.CommandMap().Bind<UtomicLoadingProgressSignal, PreloadAssetsProgressCommand>();
		}

		public void Structure(){

		}

		public void Setup() {
			this.Inject<IMenuManager>().AddMenu(new PreloaderMenu());
			this.Inject<ChangeMenuStateSignal>().Dispatch(new PreloaderState());
		}

		public void Run() {

		}
	}
}
