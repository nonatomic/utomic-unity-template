using Nonatomic.Utomic;
using UnityEngine;

namespace Nonatomic.Website {

	public class PreloadAssetsProgressCommand : ICommand {

		public void Execute(params object[] parameters){

			float percLoaded = (float) parameters[0];

			var preloadBar = this.EntityList().GetComponent<HorizontalPreloadBarComponent>();
			preloadBar.SetValue(percLoaded);
		}
	}
}
