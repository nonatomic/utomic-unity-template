using Nonatomic.Utomic;
using UnityEngine;

namespace Nonatomic.Animation {

	/**
	* All features have been ran
	*/
	public class PreloadFeatureCompleteCommand : ICommand {

		public void Execute(params object[] parameters){

			this.Inject<ChangeMenuStateSignal>().Dispatch(new DefaultState());
		}
	}
}
