//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using Nonatomic.Utomic;
using UnityEngine;

namespace Nonatomic.Animation {

	public class NinjaJumpCommand : ICommand {

		public void Execute(params object[] parameters) {

			//animate
			var animator = this.EntityList().GetComponent<Animator>(x => x.name == "Ninja");
			animator.SetTrigger("Jump");

			//physical jump
			float jumpSpeed = 1.0f;
			animator.gameObject.GetComponent<Rigidbody>().AddForce(0, 0, jumpSpeed * Time.deltaTime);
		}
	}
}
