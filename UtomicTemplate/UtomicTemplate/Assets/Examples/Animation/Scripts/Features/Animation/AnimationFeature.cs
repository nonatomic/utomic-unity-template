//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace Nonatomic.Animation {

	public class AnimationFeature : IFeature{

		public void Wire() {
			this.CommandMap().Bind<NinjaJumpSignal, NinjaJumpCommand>();
		}

		public void Structure(){

		}

		public void Setup(){


		}

		public void Run(){

			//Ninja
			var canvas = this.EntityList().GetEntity<SpriteCanvasComponent>();
			var animation = this.Inject<IAnimationFactory>().MakeAnimation("Ninja", "ninja", canvas);
			var animator = animation.GetComponent<Animator>();


			MakePlatform();

			//Key controls
			var keys = canvas.AddComponent<KeyMap>();
			keys.Add(KeyCode.Space, KeyState.Down, this.Inject<NinjaJumpSignal>());
			// keys.Add(KeyCode.W, KeyState.Down, this.Inject<NinjaClimbSignal>());
			// keys.Add(KeyCode.UpArrow, KeyState.Down, this.Inject<NinjaClimbSignal>());
			// keys.Add(KeyCode.DownArrow, KeyState.Down, this.Inject<NinjaCrouchSignal>());
			// keys.Add(KeyCode.RightArrow, KeyState.Down, this.Inject<NinjaJumpSignal>());
			// keys.Add(KeyCode.D, KeyState.Down, this.Inject<NinjaJumpSignal>());
			// keys.Add(KeyCode.LeftArrow, KeyState.Down, this.Inject<NinjaJumpSignal>());
			// keys.Add(KeyCode.A, KeyState.Down, this.Inject<NinjaJumpSignal>());
		}

		private void MakePlatform(){
			var canvas = this.EntityList().GetEntity<SpriteCanvasComponent>();
			var platform = this.Inject<ISpriteFactory>().MakeSprite("wall", canvas);
			platform.transform.localPosition = new Vector3(0f,-1.419f,0);
			platform.transform.localScale = new Vector3(2.25f,2.25f,1f);
			BoxCollider2D collider = platform.AddEntityComponent<BoxCollider2D>();
			collider.size = new Vector2(1.06f, 0.15f);
			Rigidbody2D rigid = platform.AddEntityComponent<Rigidbody2D>();
			rigid.isKinematic = true;

		}
	}
}
