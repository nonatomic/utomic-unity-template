//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using System.Collections.Generic;

namespace Nonatomic.Quoridor {

	public class QuoridorModel : IQuoridorModel {

		public List<IQuoridorPlayer> Players { get; set; }
		public float GridTileSize { get{ return 0.56f; }}
		public float GridTileGap { get{ return 0.05f; }}
		public int CurrentPlayer { get; set; }

		public int StartingWallCount {
			get { return 20; }
		}

		private List<Color> colors = new List<Color>(){
			Color.red,
			Color.cyan,
			Color.yellow,
			Color.green
		};
		public List<Color> Colors {
			get { return colors; }
		}

		private List<Vector2> startingPositions = new List<Vector2>(){
			new Vector2(4,0),
			new Vector2(4,8),
			new Vector2(0,4),
			new Vector2(8,4)
		};
		public List<Vector2> StartingPositions {
			get{ return startingPositions; }
		}

		private List<QuoridorEndZone> endZones = new List<QuoridorEndZone>(){
			new QuoridorEndZone(){ from = new Vector2(0,8), to = new Vector2(8,8)},
			new QuoridorEndZone(){ from = new Vector2(0,0), to = new Vector2(8,0)},
			new QuoridorEndZone(){ from = new Vector2(8,0), to = new Vector2(8,8)},
			new QuoridorEndZone(){ from = new Vector2(0,0), to = new Vector2(0,8)}
		};
		public List<QuoridorEndZone> EndZones{
			get{ return endZones; }
		}

		private MoveType moveType = MoveType.Movement;
		public MoveType MoveType {
			get{ return moveType; }
			set{ moveType = value; }
		}
	}
}
