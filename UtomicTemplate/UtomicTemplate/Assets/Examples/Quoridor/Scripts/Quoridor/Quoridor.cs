using UnityEngine;
using System.Collections;
using Nonatomic.Utomic;

namespace Nonatomic.Quoridor {

	public class Quoridor : MonoBehaviour {

		// Use this for initialization
		public void Awake () {

			this.AddUtomic();

			/**
			* Application wide
			*/
			this.Bind<IAppModel,AppModel>();
            
			/**
			* Features
			*
			* Any game feature that requires wiring should be wrapped
			* as a feature and added here.
			*
			* Features are responsible for wiring themselves into Utomic
			*/
			this.AddFeature<PreloaderFeature>();
			this.AddFeature<Setup2DFeature>();
			this.AddFeature<DebuggingFeature>();
			this.AddFeature<QuoridorFeature>();
			this.AddFeature<StartFeature>();
			this.AddFeature<WinFeature>();
		}
	}
}
